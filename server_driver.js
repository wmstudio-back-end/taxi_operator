'use strict';

class Driver  {
  constructor(data,ws,callback) {
    this.profile = data
    this.order = null
    this.profile.gps = null
    this.findActiveOrder()
    this.initUser()
    this.ws = ws
    callback(null,{profile:data})

    console.log({profile:data})

  }

}
Driver.prototype.findActiveOrder = function(msg){
  global.db.orders.findOne({driver:this.profile._id,status:{$in:[1,2,3]}},function(err,order){
    if (order){
      console.log('ЕСТЬ НЕ ЗАКРЫТЫЙ ЗАКАЗ',order._id)
      for (var i = 0; i < ORDERS.length; i++) {
        if (order._id.toString()==ORDERS[i].order._id.toString()){
          this.setOrder(ORDERS[i])
        }
      }
    }else{
      console.log('все заказы закрыты',err,order)
    }
  }.bind(this))
}
Driver.prototype.setOrder = function(order){
  this.order = order
  this.ws.send(JSON.stringify({method:'EVENT/createInitOrder',data:{order:this.order.order},error:null,requestId:-1}))
}
Driver.prototype.initUser = function(err,order){
  USERS[this.profile._id] = this
}
Driver.prototype.cancelOrder = function(callback){

  if (this.order){
    // this.profile.online = false
    // setTimeout(function(){
    //   this.profile.online = true
    // }.bind(this),15000)
    this.order.restartOrder()
    this.order = null
    try {
      this.ws.send(JSON.stringify({method: 'EVENT/cancelOrder', data: {}, error: null, requestId: -1}))

    }catch (e) {

      delete USERS[this.profile._id]
    }

      


  }
  callback(null,{})
  // for (var i = 0; i < ORDERS.length; i++) {
  //   if (order._id.toString()==ORDERS[i].order._id.toString()){
  //     this.setOrder(ORDERS[i])
  //   }
  // }
}

Driver.prototype.finishOrder = function(dara){
  this.order = null

}
Driver.prototype.applyOrder = function(callback){
  if (this.order){
    this.order.startWorkOrder()
    callback(null,{order:this.order.order})
  }


}

module.exports =  {Driver}


