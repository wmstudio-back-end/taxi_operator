import { Injectable, EventEmitter } from '@angular/core';
import {CookieService}    from 'ngx-cookie';
import { HttpClient,HttpHeaders,HttpRequest } from '@angular/common/http';
import {AuthService} from './auth.service';
import {ModalService} from './modal.service';

declare var io;

export interface Message {
  method: string,
  data: any,
  requestId: number,

}

@Injectable({
  providedIn: 'root'
})

export class DataService {
  ready = false
  GPS = {
    lat:null,
    lon:null
  }
  user = {
    auth : false
  }
  wsRecconect: any;
  ws: any;
  requestId: number = 0;
  private message = <Message>{};
  public req = {}
  watch =null;
  constructor(private http:HttpClient,private mS:ModalService,private cookieService: CookieService) {

    this.connectWs()

    // setInterval(this.getLatLon.bind(this),5000)
  }



  getLatLon()  {
    navigator.geolocation.getCurrentPosition(this.onSuccess.bind(this), this.onError.bind(this));
  }
  onSuccess(position) {
    this.GPS.lat =position.coords.latitude
    this.GPS.lon=position.coords.longitude
    this.send('setGPSDriver',{gps:this.GPS}).then(data=>{

    })
    // alert('Latitude: '          + position.coords.latitude          + '\n' +
    //   'Longitude: '         + position.coords.longitude         + '\n' +
    //   'Altitude: '          + position.coords.altitude          + '\n' +
    //   'Accuracy: '          + position.coords.accuracy          + '\n' +
    //   'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
    //   'Heading: '           + position.coords.heading           + '\n' +
    //   'Speed: '             + position.coords.speed             + '\n' +
    //   'Timestamp: '         + position.timestamp                + '\n');
  };

  // onError Callback receives a PositionError object
  //
  onError(error) {
    alert('code: '    + error.code    + '\n' +
      'message: ' + error.message + '\n');
  }
  connectWs() {
    console.log('____________',this.cookieService.get('tokenAdmin'));

    // this.ws = new WebSocket("ws://192.168.102.231:7001");
    this.ws = new WebSocket("ws://37.230.112.34:7001");
    this.ws.onclose = (event)=> {
      if (event.wasClean) {
        console.log('Соединение закрыто чисто');
      } else {
        console.log('Обрыв соединения'); // например, "убит" процесс сервера
      }
      console.log('Код: ' + event.code + ' причина: ' + event.reason);
      this.ready = false
      setTimeout(function() {
        this.connectWs();
      }.bind(this), 1000);
      console.log('disconnect')
    };

    this.ws.onmessage = function(res){

      this.cookieService.put('token', this.cookieService.get('tokenAdmin'), {
        expires: new Date(new Date().getTime() + 60 * 60 * 2 * 1000),
        path: '/'
      });
      var data = JSON.parse(res.data)

      console.log('RES SERVER ', data);
      if (!data.data||data.error) {
        if (data.error&&data.error.code) {
          // this.popS.show_info(this.getOptionsFromId('errors', data.error.code).text);
        }
        else if (data.error&&data.error.errorId) {
          // this.popS.show_info(this.getOptionsFromId('errors', data.error.errorId).text);
        }
        else {
          // this.popS.show_info(data.error.text || data.error.error.text);
        }

        if (!data.data) {
          return;
        }
      }

      if (this.req[data.requestId]) {
        this.req[data.requestId].emit(data.data);
      } else {
        let m = data.method.split('/');
        if (this.messages[m[1] + '/' + m[2]]){
          this.messages[m[1] + '/' + m[2]].emit(data.data);
        }else{
          console.log('INCOMING MESS',data);
        }

      }


    }.bind(this);

    this.ws.onerror = (error) =>{
      console.log("Ошибка " , error);
    };
    this.ws.onopen = function (data) {
      console.log('CONNECT',this.ready)
      this.ready = true

      if (this.cookieService.get('tokenAdmin')){
        this.send('authAdmin',{token:this.cookieService.get('tokenAdmin')}).then(data=>{

          this.user = Object.assign(this.user,data)
          this.user.auth = true
          console.log('RSP',data)
        })
      }
    }.bind(this)





  }



  send(method: string, data: any, server?: string) {
    if (!server) {
      server = 'worker';
    }

    this.requestId++;
    this.message = <Message>{
      method: method,
      data: data,
      error: null,
      requestId: this.requestId
    };
    this.req[this.requestId] = new EventEmitter();
    this.ws.send(JSON.stringify(this.message));
    console.log('SEND TO SERVER ', this.message);
    return new Promise((resolve, reject) => {
      this.req[this.requestId].subscribe((data) => {
        return resolve(data);
      });
    });
  }

  closeWS() {
  }




}
