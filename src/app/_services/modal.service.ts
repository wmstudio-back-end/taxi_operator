import { Injectable } from '@angular/core';

declare var $;
@Injectable({
    providedIn: 'root'
})
export class ModalService {
    viewPostType = {
        view:false,
        type:''
    }
    errorModal = {
        view:false,
        type:'',
        timer : null,
        text:''
    }

    constructor() {


    }
    closeErrorModal(){
        this.errorModal.view = false
        this.errorModal.text = ''
        clearTimeout(this.errorModal.timer);
    }
    error(data){
        this.errorModal.text = data
        this.errorModal.view = true
        this.errorModal.timer = setTimeout(this.closeErrorModal.bind(this),3000)
        console.log(data)
    }

}
