import {Component, OnInit} from '@angular/core';
import {DataService} from "./_services/data.service";
import {AuthService} from "./_services/auth.service";
import {ModalService} from "./_services/modal.service";
import {CookieService} from 'ngx-cookie';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  implements OnInit {
  id


    constructor(public dataService: DataService,public authService:AuthService,public mS:ModalService,private cookieService:CookieService) {


    }
    ngOnInit() {}

  login(){

    this.dataService.send('authAdmin',{code:this.id,type:1}).then(data=>{
      if (data){
        this.dataService.user = Object.assign(this.dataService.user,data)
        this.dataService.user.auth = true
        this.setSession(data['_id'])
        console.log('RSP',data)
      }

    })
  }
  setSession(token){
    console.log('PUTTTTT',token);
    this.cookieService.put('tokenAdmin', token, {
      expires: new Date(new Date().getTime() + 60 * 60 * 2 * 1000),
      path:    '/'
    })

  }

}
