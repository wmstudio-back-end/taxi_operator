import { Component, OnInit } from '@angular/core';
import {DataService} from '../../_services/data.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  history = []
  constructor(private dataService:DataService) { }

  ngOnInit() {
    this.dataService.send('getHistory',
      {

      }
    ).then(data=>{
      if (data){
        console.log('[[[[',data);
        this.history = Object.assign([],data)
      }

    })
  }

}
