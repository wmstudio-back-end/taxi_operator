import { Component, OnInit } from '@angular/core';
import {CookieService}    from "ngx-cookie";
import {DataService} from "../../_services/data.service";
declare var ymaps;
@Component({
  templateUrl: './main.html',
  styleUrls: ['./main.scss']
})
export class MainComponent implements OnInit {
  id
  createOrder = false
  myMap
  pathOrder
  tariff
  type
  length
  time
  DRIVERS = []
  ORDERS = []
  constructor(private dataService:DataService) { }

  ngOnInit() {
    this.getStatuses()
  }
  getStatuses(){
    this.dataService.send('getStatuses',{}).then(data=>{
      this.DRIVERS = data['drivers']
      this.ORDERS = data['orders']
      setTimeout(this.getStatuses.bind(this),3000)
    })
  }
  getAddress(a){
    let s = a.split(',')
    if (s[s.length-2]&&s[s.length-1]){
      return s[s.length-2]+s[s.length-1]
    }
    return a
  }


}
