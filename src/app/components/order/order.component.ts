import { Component, OnInit } from '@angular/core';
import {DataService} from '../../_services/data.service';
import {Router} from '@angular/router';
declare var ymaps;
@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  createOrder = false
  myMap
  pathOrder
  tariff
  type
  length
  time
  resultPrice
  constructor(private dataService:DataService, public router: Router) { }

  ngOnInit() {
    this.initMap()
  }

  initOrder(){
    // this.createOrder = !this.createOrder
    this.initMap()



  }
  clearMap(){

    this.myMap.destroy();
    this.myMap = null;
  }
  calculateOrder(length,time,pathOrder){
    this.length = length
    this.time = time
    this.pathOrder = pathOrder
    this.viweOrder()
  }
  viweOrder(){


    console.log('length',this.length);
    console.log('time',this.time);
    console.log('tariff',this.tariff);
    console.log('type',this.type);
    if (this.length&&this.time&&this.tariff&&this.type){
      this.dataService.send('calculateOrder',
        {
          length:this.length,
          time:this.time,
          tariff:this.tariff,
          type:this.type,
        }
      ).then(data=>{
        if (data){
          this.resultPrice = data['price']
          console.log('RSP',data)
        }

      })
    }




  }
  createInitOrder(){
  if (!this.pathOrder){ return}
    var Order = {
      length:this.length,
      time:this.time,
      tariff:this.tariff,
      type:this.type,
      path:[{
        address:this.pathOrder[0].address,
        coordinates:this.pathOrder[0].coordinates
      },
        {
          address:this.pathOrder[1].address,
          coordinates:this.pathOrder[1].coordinates
        }]
    }
    if (this.length&&this.time&&this.tariff&&this.type&&this.pathOrder[0]&&this.pathOrder[1]){
      this.dataService.send('createInitOrder',Order).then(data=>{
        if (data){
          this.router.navigate(['/main']);
        }

      })
    }

  }
  initMap(){
    console.log("INIT MAP")
    var $this = this
    // Стоимость за километр.
    var DELIVERY_TARIFF = 20
    // Минимальная стоимость.
    var MINIMUM_COST = 200
    this.myMap = new ymaps.Map('map', {
      center: [56.846526437023876,53.227313794677755],
      zoom: 12,
      controls: []
    })
    // Создадим панель маршрутизации.
    var routePanelControl = new ymaps.control.RoutePanel({
      options: {
        position:{
          bottom:20
        },
        // Добавим заголовок панели.
        showHeader: true,
        maxWidth:400,
        title: 'Расчёт доставки'
      }
    })
    var zoomControl = new ymaps.control.ZoomControl({
      options: {
        size: 'large',
        float: 'none',
        position: {
          bottom: 145,
          right: 10
        }
      }
    });
    // Пользователь сможет построить только автомобильный маршрут.
    routePanelControl.routePanel.options.set({
      types: {auto: true}
    });
    var searchControl = new ymaps.control.SearchControl({
      options: {
        boundedBy:[[55.87540878591494,49.76571751072512],[58.60788166101351,55.203949932600096]],
        // fitMaxWidth:true,
        // noPlacemark: true,
        strictBounds: true,
        // kind:'house'
      }});
    var searchControl2 = new ymaps.control.SearchControl({
      options: {
        boundedBy:[[55.87540878591494,49.76571751072512],[58.60788166101351,55.203949932600096]],
        float:'right',
        // noPlacemark: true,
        strictBounds: true,
        // kind:'house'
      }});
    // var mBounds = this.myMap.getBounds();
    // var searchControl = new ymaps.control.SearchControl({ boundedBy: mBounds });
    console.log('================================================')
    // var searchControl = new ymaps.SuggestView('suggest', {
    //   boundedBy : [[55.87540878591494,49.76571751072512],[58.60788166101351,55.203949932600096]],
    //   noPlacemark: true,
    //   strictBounds: true,
    //   kind:'house'
    // })
    // Если вы хотите задать неизменяемую точку "откуда", раскомментируйте код ниже.
    /*routePanelControl.routePanel.state.set({
        fromEnabled: false,
        from: 'Москва, Льва Толстого 16'
     });*/


    this.myMap.controls.add(routePanelControl)
    this.myMap.controls.add(zoomControl)
    // this.myMap.controls.add(searchControl);
    // this.myMap.controls.add(searchControl2);

    // Получим ссылку на маршрут.
    routePanelControl.routePanel.getRouteAsync().then(function (route) {

      // Зададим максимально допустимое число маршрутов, возвращаемых мультимаршрутизатором.
      route.model.setParams({results: 1}, true);

      // Повесим обработчик на событие построения маршрута.
      // route.model.events.
      route.model.events.add('requestsuccess', function (event) {

        var activeRoute = route.getActiveRoute();
        if (activeRoute) {

          // console.log(route.model.getJson().properties.waypoints[0].address);
          // console.log(route.model.getJson().properties.waypoints[0].coordinates);


          // Получим протяженность маршрута.
          var length = route.getActiveRoute().properties.get("distance")
          var time = route.getActiveRoute().properties.get("durationInTraffic")
          $this.calculateOrder(length.value,time.value,route.model.getJson().properties.waypoints)
          console.log('length',time)
          // Вычислим стоимость доставки.
          var price = calculate(Math.round(length.value / 1000))
          // Создадим макет содержимого балуна маршрута.
          var balloonContentLayout = ymaps.templateLayoutFactory.createClass(
            '<span>Расстояние: ' + length.text + '.</span><br/>' +
            '<span>Время в пути: ' + time.text + '.</span><br/>'
            // '<span style="font-weight: bold; font-style: italic">Стоимость доставки: ' + price + ' р.</span>'
          );
          // Зададим этот макет для содержимого балуна.
          route.options.set('routeBalloonContentLayout', balloonContentLayout);
          // Откроем балун.
          activeRoute.balloon.open();
        }
      });

    });
    // Функция, вычисляющая стоимость доставки.
    function calculate(routeLength) {
      return Math.max(routeLength * DELIVERY_TARIFF, MINIMUM_COST);
    }
  }

}
