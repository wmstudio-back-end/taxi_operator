import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {MainComponent} from "./components/main/main";
import {HistoryComponent} from './components/history/history.component';
import {OrderComponent} from './components/order/order.component';



const routes: Routes = [
    {path:'', component:MainComponent},
    {path:'main', component:MainComponent},
    {path:'history', component:HistoryComponent},
    {path:'order', component:OrderComponent},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
