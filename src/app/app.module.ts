import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA,NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {DataService} from './_services/data.service';
import {CookieModule, CookieService} from 'ngx-cookie';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';




import {APP_BASE_HREF} from '@angular/common';

import {MainComponent} from './components/main/main';

import {ModalService} from './_services/modal.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import { HistoryComponent } from './components/history/history.component';
import { NavigateComponent } from './components/navigate/navigate.component';
import { OrderComponent } from './components/order/order.component';





@NgModule({
  declarations: [
    AppComponent,

    MainComponent,

    HistoryComponent,

    NavigateComponent,

    OrderComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    AppRoutingModule,
    CookieModule.forRoot(),



  ],
  providers: [
      DataService,
    ModalService,
      CookieService,

    {provide: APP_BASE_HREF, useValue : '/' }

  ],
  exports: [MatButtonModule, MatCheckboxModule],
    schemas:      [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
